package com.example.mangaonline.utils;

import android.app.Application;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.Environment;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;

import com.github.barteksc.pdfviewer.PDFView;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FileDownloadTask;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageMetadata;
import com.google.firebase.storage.StorageReference;

import java.io.File;
import java.io.FileOutputStream;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Locale;

public class MyApplication extends Application {
  private static final String TAG_DOWNLOAD = "DOWNLOAD_TAG";

  @Override
  public void onCreate() {
    super.onCreate();
  }

  public static String formatTimestamp(long timestamp) {
    Calendar cal = Calendar.getInstance(Locale.ENGLISH);
    cal.setTimeInMillis(timestamp);
    return DateFormat.format("dd/MM/yyyy", cal).toString();
  }

  public static void loadPdfFromUrlSinglePage(String pdfUrl, String pdfTitle, PDFView pdfView,
                                              ProgressBar progressBar, TextView pagesTv) {
    String TAG = "PDF_LOAD_SINGLE_TAG";

    StorageReference ref = FirebaseStorage.getInstance().getReferenceFromUrl(pdfUrl);
    ref.getBytes(Constants.MAX_BYTES_PDF)
        .addOnSuccessListener(bytes -> {
          Log.d(TAG, "onSuccess: " + pdfTitle + " successfully got the file");

          pdfView.fromBytes(bytes)
              .pages(0)
              .spacing(0)
              .swipeHorizontal(false)
              .enableSwipe(false)
              .onError(t -> {
                progressBar.setVisibility(View.INVISIBLE);
                Log.d(TAG, "onError: " + t.getMessage());
              })
              .onPageError((page, t) -> {
                progressBar.setVisibility(View.INVISIBLE);
                Log.d(TAG, "onPageError: " + t.getMessage());
              })
              .onLoad(nbPages -> {
                progressBar.setVisibility(View.INVISIBLE);
                if (pagesTv != null) {
                  pagesTv.setText(String.valueOf(nbPages));
                }
              })
              .load();
        })
        .addOnFailureListener(e -> {
          progressBar.setVisibility(View.INVISIBLE);
          Log.d(TAG, "onFailure: failed getting file from url due to " + e.getMessage());
        });
  }

  public static void loadPdfSize(String pdfUrl, String pdfTitle, TextView tvSize) {
    String TAG = "PDF_SIZE_TAG";

    StorageReference ref = FirebaseStorage.getInstance().getReferenceFromUrl(pdfUrl);
    ref.getMetadata()
        .addOnSuccessListener(new OnSuccessListener<StorageMetadata>() {
          @Override
          public void onSuccess(StorageMetadata storageMetadata) {
            double bytes = storageMetadata.getSizeBytes();
            Log.d(TAG, "onSuccess: " + pdfTitle + " " + bytes);

            double kb = bytes / 1024;
            double mb = kb / 1024;

            if (mb >= 1) {
              tvSize.setText(String.format("%.2f", mb) + " MB");
            } else if (kb >= 1) {
              tvSize.setText(String.format("%.2f", kb) + " KB");
            } else {
              tvSize.setText(String.format("%.2f", bytes) + " bytes");
            }
          }
        })
        .addOnFailureListener(new OnFailureListener() {
          @Override
          public void onFailure(@NonNull Exception e) {
            Log.d(TAG, "onFailure: " + e.getMessage());
          }
        });
  }

  public static void loadCategory(String categoryId, TextView categoryTv) {
    DatabaseReference ref = FirebaseDatabase.getInstance().getReference("Categories");
    ref.child(categoryId).addListenerForSingleValueEvent(new ValueEventListener() {
      @Override
      public void onDataChange(@NonNull DataSnapshot snapshot) {
        String category = "" + snapshot.child("category").getValue();
        categoryTv.setText(category);
      }

      @Override
      public void onCancelled(@NonNull DatabaseError error) {

      }
    });

  }

  public static void incrementBookViewCount(String bookId) {
    DatabaseReference ref = FirebaseDatabase.getInstance().getReference("Books");
    ref.child(bookId)
        .addListenerForSingleValueEvent(new ValueEventListener() {
          @Override
          public void onDataChange(@NonNull DataSnapshot snapshot) {
            String viewsCount = "" + snapshot.child("viewsCount").getValue();
            if (viewsCount.equals("") || viewsCount.equals("null")) {
              viewsCount = "0";
            }

            long newViewsCount = Long.parseLong(viewsCount) + 1;
            HashMap<String, Object> hashMap = new HashMap<>();
            hashMap.put("viewsCount", newViewsCount);

            DatabaseReference reference = FirebaseDatabase.getInstance().getReference("Books");
            reference.child(bookId)
                .updateChildren(hashMap);
          }

          @Override
          public void onCancelled(@NonNull DatabaseError error) {

          }
        });
  }

  public static void downloadBook(Context context, String bookId, String bookTitle,
                                  String bookUrl) {
    Log.d(TAG_DOWNLOAD, "downloadBook: downloading book .... " +bookUrl +"//"+bookId);
    String nameWithExtention = bookTitle + ".pdf";
    ProgressDialog progressDialog = new ProgressDialog(context);
    progressDialog.setTitle("Please wait");
    progressDialog.setMessage("Downloading" + nameWithExtention + ".....");
    progressDialog.setCanceledOnTouchOutside(false);
    progressDialog.show();
    StorageReference ref = FirebaseStorage.getInstance().getReferenceFromUrl(bookUrl);
    saveDownloadedBook(context, progressDialog, nameWithExtention, bookId, ref);

  }

  private static void saveDownloadedBook(Context context, ProgressDialog progressDialog, String nameWithExtention, String bookId, StorageReference pathRef) {
    try {
      File localFile = File.createTempFile("book", nameWithExtention);
      pathRef.getFile(localFile)
                .addOnSuccessListener(new OnSuccessListener<FileDownloadTask.TaskSnapshot>() {
                                          @Override
                                          public void onSuccess(FileDownloadTask.TaskSnapshot taskSnapshot) {
                                              Toast.makeText(context, "Save to download", Toast.LENGTH_SHORT).show();
                                              progressDialog.dismiss();
                                              incrementBookDownloadCount(bookId);
                                          }
                                      });
    } catch (Exception e) {
      Toast.makeText(context, "False", Toast.LENGTH_SHORT).show();
    }
  }

  private static void incrementBookDownloadCount(String bookId) {
    DatabaseReference ref = FirebaseDatabase.getInstance().getReference("Books");
    ref.child(bookId).addListenerForSingleValueEvent(
        new ValueEventListener() {
          @Override
          public void onDataChange(@NonNull DataSnapshot snapshot) {
            String downloadsCount = "" + snapshot.child("downloadsCount").getValue();

            if (downloadsCount.equals("") || downloadsCount.equals("null")) {
              downloadsCount = "0";
            }

            long newDownLoadCount = Long.parseLong(downloadsCount) + 1;
            HashMap<String, Object> hashMap = new HashMap<>();
            hashMap.put("downloadsCount", newDownLoadCount);

            DatabaseReference reference = FirebaseDatabase.getInstance().getReference("Books");
            reference.child(bookId).updateChildren(hashMap);
          }

          @Override
          public void onCancelled(@NonNull DatabaseError error) {

          }
        }
    );
  }

  public static void addToFavorite(Context context, String bookId) {
    FirebaseAuth firebaseAuth = FirebaseAuth.getInstance();
    if (firebaseAuth.getCurrentUser() == null) {
      Toast.makeText(context, "You're not logged in", Toast.LENGTH_SHORT).show();
    } else {
      long timestamp = System.currentTimeMillis();

      HashMap<String, Object> hashMap = new HashMap<>();
      hashMap.put("bookId", "" + bookId);
      hashMap.put("timestamp", "" + timestamp);

      DatabaseReference ref = FirebaseDatabase.getInstance().getReference("Users");
      ref.child(firebaseAuth.getUid()).child("Favorites").child(bookId)
          .setValue(hashMap)
          .addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void unused) {
              Toast.makeText(context, "Added to your favorites list...", Toast.LENGTH_SHORT).show();
            }
          })
          .addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
              Toast.makeText(context, "Failed to add to favorite due to" + e.getMessage(),
                  Toast.LENGTH_SHORT).show();
            }
          });
    }
  }


  public static void removeFromFavorite(Context context, String bookId) {
    FirebaseAuth firebaseAuth = FirebaseAuth.getInstance();
    if (firebaseAuth.getCurrentUser() == null) {
      Toast.makeText(context, "You're not logged in", Toast.LENGTH_SHORT).show();
    } else {
      DatabaseReference ref = FirebaseDatabase.getInstance().getReference("Users");
      ref.child(firebaseAuth.getUid()).child("Favorites").child(bookId)
          .removeValue()
          .addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void unused) {
              Toast.makeText(context, "Remove from your favorites list...", Toast.LENGTH_SHORT).show();
            }
          })
          .addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
              Toast.makeText(context, "Failed to remove from favorite due to" + e.getMessage(),
                  Toast.LENGTH_SHORT).show();
            }
          });
    }
  }

  public static void deleteBook(Context context, String bookId, String bookUrl, String bookTitle) {
    String TAG = "DELETE_BOOK_TAG";

    Log.d(TAG, "deleteBook: Deleting...");
    ProgressDialog progressDialog = new ProgressDialog(context);
    progressDialog.setMessage("Please wait");
    progressDialog.setMessage("Deleting " + bookTitle + " ...");
    progressDialog.show();

    Log.d(TAG, "deleteBook: Deleting from storage...");
    StorageReference storageReference = FirebaseStorage.getInstance().getReferenceFromUrl(bookUrl);
    storageReference.delete()
        .addOnSuccessListener(new OnSuccessListener<Void>() {
          @Override
          public void onSuccess(Void unused) {
            Log.d(TAG, "onSuccess: Deleted from storage");
            Log.d(TAG, "onSuccess: Now deleting info from db");
            DatabaseReference reference = FirebaseDatabase.getInstance().getReference("Books");
            reference.child(bookId)
                .removeValue()
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                  @Override
                  public void onSuccess(Void unused) {
                    Log.d(TAG, "onSuccess: Deleted from db too");
                    progressDialog.dismiss();
                    Toast.makeText(context, "Book Deleted Successfully", Toast.LENGTH_SHORT).show();
                  }
                })
                .addOnFailureListener(new OnFailureListener() {
                  @Override
                  public void onFailure(@NonNull Exception e) {
                    Log.d(TAG, "onFailure: Failed to delete from db due to" + e.getMessage());
                    progressDialog.dismiss();
                    Toast.makeText(context, "" + e.getMessage(), Toast.LENGTH_SHORT).show();
                  }
                });
          }
        })
        .addOnFailureListener(new OnFailureListener() {
          @Override
          public void onFailure(@NonNull Exception e) {
            Log.d(TAG, "onFailure: Failed to delete from storage due to " + e.getMessage());
            progressDialog.dismiss();
            Toast.makeText(context, "" + e.getMessage(), Toast.LENGTH_SHORT).show();
          }
        });
  }
}
