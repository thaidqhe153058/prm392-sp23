package com.example.mangaonline.adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.mangaonline.activities.PdfListUserActivity;
import com.example.mangaonline.databinding.RowCategoryAdminBinding;
import com.example.mangaonline.databinding.RowCategoryUserBinding;
import com.example.mangaonline.filters.FilterCategoryUser;
import com.example.mangaonline.models.Category;

import java.util.List;

public class AdapterCategoryUser extends RecyclerView.Adapter<AdapterCategoryUser.CategoryHolder> implements Filterable {

  private Context context;
  private List<Category> categoryList;
  private List<Category> filteredList;
  private FilterCategoryUser filter;
  private RowCategoryUserBinding binding;

  public AdapterCategoryUser(Context context, List<Category> categoryList) {
    this.context = context;
    this.categoryList = categoryList;
    this.filteredList = categoryList;
  }

  public void setCategoryList(List<Category> categoryList) {
    this.categoryList = categoryList;
  }

  @NonNull
  @Override
  public CategoryHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
    binding = RowCategoryUserBinding.inflate(LayoutInflater.from(context), parent, false);
    return new CategoryHolder(binding.getRoot());
  }

  @Override
  public void onBindViewHolder(@NonNull CategoryHolder holder, int position) {
    Category model = categoryList.get(position);
    String id = model.getId();
    String category = model.getCategory();
    String uid = model.getUid();
    long timestamp = model.getTimestamp();

    holder.tvCategoryTitle.setText(category);

    holder.itemView.setOnClickListener(v -> {
      Intent intent = new Intent(context, PdfListUserActivity.class);
      intent.putExtra("categoryId", id);
      intent.putExtra("categoryTitle", category);
      context.startActivity(intent);
    });
  }

  @Override
  public int getItemCount() {
    return categoryList.size();
  }

  @Override
  public Filter getFilter() {
    if (filter == null) {
      filter = new FilterCategoryUser(filteredList, this);
    }
    return filter;
  }

  class CategoryHolder extends RecyclerView.ViewHolder {
    private TextView tvCategoryTitle;

    public CategoryHolder(@NonNull View itemView) {
      super(itemView);
      tvCategoryTitle = binding.tvCategoryTitle;
    }
  }

}
