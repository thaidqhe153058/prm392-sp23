package com.example.mangaonline.adapters;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.mangaonline.activities.PdfListAdminActivity;
import com.example.mangaonline.databinding.RowCategoryAdminBinding;
import com.example.mangaonline.filters.FilterCategoryAdmin;
import com.example.mangaonline.models.Category;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.List;

public class AdapterCategoryAdmin extends RecyclerView.Adapter<AdapterCategoryAdmin.CategoryHolder> implements Filterable {

  private Context context;
  private List<Category> categoryList;
  private List<Category> filteredList;
  private FilterCategoryAdmin filter;
  private RowCategoryAdminBinding binding;

  public AdapterCategoryAdmin(Context context, List<Category> categoryList) {
    this.context = context;
    this.categoryList = categoryList;
    this.filteredList = categoryList;
  }

  public void setCategoryList(List<Category> categoryList) {
    this.categoryList = categoryList;
  }

  @NonNull
  @Override
  public CategoryHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
    binding = RowCategoryAdminBinding.inflate(LayoutInflater.from(context), parent, false);
    return new CategoryHolder(binding.getRoot());
  }

  @Override
  public void onBindViewHolder(@NonNull CategoryHolder holder, int position) {
    Category model = categoryList.get(position);
    String id = model.getId();
    String category = model.getCategory();
    String uid = model.getUid();
    long timestamp = model.getTimestamp();

    holder.tvCategoryTitle.setText(category);

    holder.btnDeleteCate.setOnClickListener(v -> {
      AlertDialog.Builder builder = new AlertDialog.Builder(context);
      builder.setTitle("Delete Category")
          .setMessage("Are you sure?")
          .setPositiveButton("Confirm", (dialog, which) -> {
            Toast.makeText(context, "Deleting Category...", Toast.LENGTH_SHORT).show();
            deleteCategory(model);
          })
          .setNegativeButton("Cancel", (dialog, which) -> dialog.dismiss())
          .show();
    });

    holder.itemView.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        Intent intent = new Intent(context, PdfListAdminActivity.class);
        intent.putExtra("categoryId", id);
        intent.putExtra("categoryTitle", category);
        context.startActivity(intent);
      }
    });
  }

  private void deleteCategory(Category model) {
    String id = model.getId();
    DatabaseReference ref = FirebaseDatabase.getInstance().getReference("Categories");
    ref.child(id)
        .removeValue()
        .addOnSuccessListener(unused -> {
          Toast.makeText(context, "Delete Successful", Toast.LENGTH_SHORT).show();
        })
        .addOnFailureListener(e -> {
          Toast.makeText(context, "Delete Failed: " + e.getMessage(), Toast.LENGTH_SHORT).show();
        });
  }

  @Override
  public int getItemCount() {
    return categoryList.size();
  }

  @Override
  public Filter getFilter() {
    if (filter == null) {
      filter = new FilterCategoryAdmin(filteredList, this);
    }
    return filter;
  }

  class CategoryHolder extends RecyclerView.ViewHolder {
    private TextView tvCategoryTitle;
    private ImageButton btnDeleteCate;

    public CategoryHolder(@NonNull View itemView) {
      super(itemView);
      tvCategoryTitle = binding.tvCategoryTitle;
      btnDeleteCate = binding.btnDeleteCategory;
    }
  }

}
