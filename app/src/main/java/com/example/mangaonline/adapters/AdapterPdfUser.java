package com.example.mangaonline.adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.mangaonline.R;
import com.example.mangaonline.activities.PdfDetailActivity;
import com.example.mangaonline.filters.FilterPdfUser;
import com.example.mangaonline.models.Pdf;
import com.example.mangaonline.utils.MyApplication;
import com.github.barteksc.pdfviewer.PDFView;

import java.util.List;

public class AdapterPdfUser extends RecyclerView.Adapter<AdapterPdfUser.HolderPdfUser> implements Filterable {

  private Context context;
  public List<Pdf> pdfList;
  public List<Pdf> filterList;
  private FilterPdfUser filter;

  public AdapterPdfUser(Context context, List<Pdf> pdfList) {
    this.context = context;
    this.pdfList = pdfList;
    this.filterList = pdfList;
  }

  @NonNull
  @Override
  public HolderPdfUser onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
    View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_pdf_user, parent, false);
    return new HolderPdfUser(v);
  }

  @Override
  public void onBindViewHolder(@NonNull HolderPdfUser holder, int position) {
    Pdf model = pdfList.get(position);
    String bookId = model.getId();
    String title = model.getTitle();
    String desc = model.getDescription();
    String pdfUrl = model.getUrl();
    String categoryId = model.getCategoryId();
    String pdfId = model.getId();
    long timestamp = model.getTimestamp();

    String date = MyApplication.formatTimestamp(timestamp);

    holder.tvTitle.setText(title);
    holder.tvDesc.setText(desc);
    holder.tvDate.setText(date);

    MyApplication.loadPdfFromUrlSinglePage(pdfUrl, title, holder.pdfView, holder.progressBar, null);
    MyApplication.loadCategory("" + categoryId, holder.tvCategory);
    MyApplication.loadPdfSize(pdfUrl, title, holder.tvSize);

    holder.itemView.setOnClickListener(v -> {
      Intent intent = new Intent(context, PdfDetailActivity.class);
      intent.putExtra("bookId", bookId);
      context.startActivity(intent);
    });
  }

  @Override
  public int getItemCount() {
    return pdfList.size();
  }

  @Override
  public Filter getFilter() {
    if (filter == null) {
      filter = new FilterPdfUser(filterList, this);
    }
    return filter;
  }

  class HolderPdfUser extends RecyclerView.ViewHolder {

    private TextView tvTitle;
    private TextView tvDesc;
    private TextView tvCategory;
    private TextView tvSize;
    private TextView tvDate;
    private PDFView pdfView;
    private ProgressBar progressBar;

    public HolderPdfUser(@NonNull View itemView) {
      super(itemView);
      tvTitle = itemView.findViewById(R.id.tv_title_pdf_user);
      tvDesc = itemView.findViewById(R.id.tv_desc_pdf_user);
      tvCategory = itemView.findViewById(R.id.tv_category_user);
      tvSize = itemView.findViewById(R.id.tv_size_user);
      tvDate = itemView.findViewById(R.id.tv_date_user);
      pdfView = itemView.findViewById(R.id.pdfView_user);
      progressBar = itemView.findViewById(R.id.progress_bar_user);
    }
  }
}
