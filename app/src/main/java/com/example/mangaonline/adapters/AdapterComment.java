package com.example.mangaonline.adapters;

import android.content.Context;
import android.content.DialogInterface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.mangaonline.R;
import com.example.mangaonline.databinding.RowCmtBinding;
import com.example.mangaonline.models.Comment;
import com.example.mangaonline.utils.MyApplication;
import com.google.android.material.imageview.ShapeableImageView;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

public class AdapterComment extends RecyclerView.Adapter<AdapterComment.HolderComment> {

  private Context context;
  private ArrayList<Comment> commentArrayList;

  private FirebaseAuth firebaseAuth;

  private RowCmtBinding binding;

  public AdapterComment(Context context, ArrayList<Comment> commentArrayList) {
    this.context = context;
    this.commentArrayList = commentArrayList;
    firebaseAuth = FirebaseAuth.getInstance();
  }

  @NonNull
  @Override
  public HolderComment onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
    binding = RowCmtBinding.inflate(LayoutInflater.from(context), parent, false);
    return new HolderComment(binding.getRoot());
  }

  @Override
  public void onBindViewHolder(@NonNull HolderComment holder, int position) {
    Comment modelComment = commentArrayList.get(position);
    String id = modelComment.getId();
    String bookId = modelComment.getBookId();
    String comment = modelComment.getComment();
    String uid = modelComment.getUid();
    String timestamp = modelComment.getTimestamp();

    String date = MyApplication.formatTimestamp(Long.parseLong(timestamp));

    holder.dateTv.setText(date);
    holder.commentTv.setText(comment);

    loadUserDetails(modelComment, holder);

    holder.itemView.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        if (firebaseAuth.getCurrentUser() != null && uid.equals((firebaseAuth.getUid()))) {
          deleteComment(modelComment, holder);
        }
      }
    });
  }

  private void deleteComment(Comment modelComment, HolderComment holder) {
    AlertDialog.Builder builder = new AlertDialog.Builder(context);
    builder.setTitle("Delete comment")
        .setMessage("Are you sure?")
        .setPositiveButton("DELETE", new DialogInterface.OnClickListener() {
          @Override
          public void onClick(DialogInterface dialogInterface, int i) {
            DatabaseReference ref = FirebaseDatabase.getInstance().getReference("Books");
            ref.child(modelComment.getBookId())
                .child("Comments")
                .child(modelComment.getId())
                .removeValue()
                .addOnSuccessListener(unused -> {
                  Toast.makeText(context, "Deleted Successful", Toast.LENGTH_SHORT).show();
                })
                .addOnFailureListener(e -> {
                  Toast.makeText(context, "Deleted Failed: " + e.getMessage(),
                      Toast.LENGTH_SHORT).show();
                });
          }
        })
        .setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
          @Override
          public void onClick(DialogInterface dialogInterface, int i) {
            dialogInterface.dismiss();
          }
        })
        .show();
  }

  private void loadUserDetails(Comment modelComment, HolderComment holder) {
    String uid = modelComment.getUid();

    DatabaseReference red = FirebaseDatabase.getInstance().getReference("Users");
    red.child(uid)
        .addListenerForSingleValueEvent(new ValueEventListener() {
          @Override
          public void onDataChange(@NonNull DataSnapshot snapshot) {
            String name = "" + snapshot.child("name").getValue();
            String profileImage = "" + snapshot.child("avatar").getValue();

            holder.nameTv.setText(name);
            try {
              Glide.with(context)
                  .load(profileImage)
                  .placeholder(R.drawable.ic_person_gray)
                  .into(holder.profileIv);
            } catch (Exception e) {
              holder.profileIv.setImageResource(R.drawable.ic_person_gray);
            }
          }

          @Override
          public void onCancelled(@NonNull DatabaseError error) {

          }
        });
  }

  @Override
  public int getItemCount() {
    return commentArrayList.size();
  }

  class HolderComment extends RecyclerView.ViewHolder {
    ShapeableImageView profileIv;
    TextView nameTv, dateTv, commentTv;

    public HolderComment(@NonNull View itemView) {
      super(itemView);
      profileIv = binding.profileIv;
      nameTv = binding.nameTv;
      dateTv = binding.dateTv;
      commentTv = binding.commentTv;
    }
  }
}
