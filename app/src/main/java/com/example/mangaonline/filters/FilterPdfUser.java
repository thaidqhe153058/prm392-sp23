package com.example.mangaonline.filters;

import android.widget.Filter;

import com.example.mangaonline.adapters.AdapterPdfUser;
import com.example.mangaonline.models.Pdf;

import java.util.ArrayList;
import java.util.List;

public class FilterPdfUser extends Filter {

  private List<Pdf> filterList;
  private AdapterPdfUser adapterPdfUser;

  public FilterPdfUser(List<Pdf> filterList, AdapterPdfUser adapterPdfUser) {
    this.filterList = filterList;
    this.adapterPdfUser = adapterPdfUser;
  }

  @Override
  protected FilterResults performFiltering(CharSequence constraint) {
    FilterResults results = new FilterResults();
    if (constraint != null || constraint.length() > 0) {
      constraint = constraint.toString().toUpperCase();
      List<Pdf> filteredModels = new ArrayList<>();

      for (int i = 0; i < filterList.size(); i++) {
        if (filterList.get(i).getTitle().toUpperCase().contains(constraint)) {
          filteredModels.add(filterList.get(i));
        }
      }

      results.count = filteredModels.size();
      results.values = filteredModels;
    } else {
      results.count = filterList.size();
      results.values = filterList;
    }
    return results;
  }

  @Override
  protected void publishResults(CharSequence constraint, FilterResults results) {
    adapterPdfUser.pdfList = (List<Pdf>) results.values;
    adapterPdfUser.notifyDataSetChanged();
  }
}
