package com.example.mangaonline.filters;

import android.widget.Filter;

import com.example.mangaonline.adapters.AdapterCategoryAdmin;
import com.example.mangaonline.adapters.AdapterCategoryUser;
import com.example.mangaonline.models.Category;

import java.util.ArrayList;
import java.util.List;

public class FilterCategoryUser extends Filter {

  private List<Category> categoryList;

  private AdapterCategoryUser adapter;

  public FilterCategoryUser(List<Category> categoryList, AdapterCategoryUser adapter) {
    this.categoryList = categoryList;
    this.adapter = adapter;
  }

  @Override
  protected FilterResults performFiltering(CharSequence constraint) {
    FilterResults results = new FilterResults();
    if (constraint != null && constraint.length() > 0) {
      constraint = constraint.toString().toUpperCase();
      List<Category> filteredList = new ArrayList<>();
      for (int i = 0; i < categoryList.size(); i++) {
        if (categoryList.get(i).getCategory().toUpperCase().contains(constraint)) {
          filteredList.add(categoryList.get(i));
        }
      }
      results.count = filteredList.size();
      results.values = filteredList;
    } else {
      results.count = categoryList.size();
      results.values = categoryList;
    }
    return results;
  }

  @Override
  protected void publishResults(CharSequence constraint, FilterResults results) {
    adapter.setCategoryList((List<Category>) results.values);
    adapter.notifyDataSetChanged();
  }
}
