package com.example.mangaonline.filters;

import android.widget.Filter;

import com.example.mangaonline.adapters.AdapterPdfAdmin;
import com.example.mangaonline.models.Pdf;

import java.util.ArrayList;
import java.util.List;

public class FilterPdfAdmin extends Filter {

  private List<Pdf> filterList;

  private AdapterPdfAdmin adapter;

  public FilterPdfAdmin(List<Pdf> categoryList, AdapterPdfAdmin adapter) {
    this.filterList = categoryList;
    this.adapter = adapter;
  }

  @Override
  protected FilterResults performFiltering(CharSequence constraint) {
    FilterResults results = new FilterResults();
    if (constraint != null && constraint.length() > 0) {
      constraint = constraint.toString().toUpperCase();
      List<Pdf> filteredList = new ArrayList<>();
      for (int i = 0; i < filterList.size(); i++) {
        if (filterList.get(i).getTitle().toUpperCase().contains(constraint)) {
          filteredList.add(filterList.get(i));
        }
      }
      results.count = filteredList.size();
      results.values = filteredList;
    } else {
      results.count = filterList.size();
      results.values = filterList;
    }
    return results;
  }

  @Override
  protected void publishResults(CharSequence constraint, FilterResults results) {
    adapter.pdfArrayList = (ArrayList<Pdf>) results.values;
    adapter.notifyDataSetChanged();
  }
}
