package com.example.mangaonline.activities;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

import androidx.appcompat.app.AppCompatActivity;

import com.example.mangaonline.R;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class SplashActivity extends AppCompatActivity {

  private FirebaseAuth firebaseAuth;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_splash);

    firebaseAuth = FirebaseAuth.getInstance();

    new Handler().postDelayed(() -> {
      checkUser();
    }, 2000);
  }

  private void checkUser() {
    FirebaseUser firebaseUser = firebaseAuth.getCurrentUser();

    if (firebaseUser == null) {
      startActivity(new Intent(SplashActivity.this, MainActivity.class));
      finish();
    } else {
      DatabaseReference ref = FirebaseDatabase.getInstance().getReference("Users");
      ref.child(firebaseAuth.getUid()).addListenerForSingleValueEvent(new ValueEventListener() {
        @Override
        public void onDataChange(DataSnapshot snapshot) {
          String userType = (String) snapshot.child("type").getValue();

          if (userType.equals("admin")) {
            startActivity(new Intent(SplashActivity.this, DashboardAdminActivity.class));
            finish();
          } else if (userType.equals("user")) {
            startActivity(new Intent(SplashActivity.this, DashboardUserActivity.class));
            finish();
          }
        }

        @Override
        public void onCancelled(DatabaseError error) {
        }
      });
    }
  }

}