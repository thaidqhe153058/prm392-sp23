package com.example.mangaonline.activities;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.example.mangaonline.R;
import com.example.mangaonline.adapters.AdapterCategoryUser;
import com.example.mangaonline.databinding.ActivityDashboardUserBinding;
import com.example.mangaonline.models.Category;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

public class DashboardUserActivity extends AppCompatActivity {
  public List<Category> categoryList;

  private ActivityDashboardUserBinding binding;

  private FirebaseAuth firebaseAuth;

  private TextView tvDashUserSubTitle;
  private AdapterCategoryUser categoryAdapter;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    binding = ActivityDashboardUserBinding.inflate(getLayoutInflater());
    setContentView(binding.getRoot());

    firebaseAuth = FirebaseAuth.getInstance();

    tvDashUserSubTitle = findViewById(R.id.tv_dash_user_sub_title);

    checkUser();
    loadCategories();

    binding.edtSearchBook.addTextChangedListener(new TextWatcher() {
      @Override
      public void beforeTextChanged(CharSequence s, int start, int count, int after) {
      }

      @Override
      public void onTextChanged(CharSequence s, int start, int before, int count) {
        try {
          categoryAdapter.getFilter().filter(s);
        } catch (Exception ignored) {
        }
      }

      @Override
      public void afterTextChanged(Editable s) {
      }
    });

    findViewById(R.id.imb_logout_user).setOnClickListener(v -> {
      firebaseAuth.signOut();
      startActivity(new Intent(DashboardUserActivity.this, MainActivity.class));
      finish();
    });

    findViewById(R.id.imb_profile_user).setOnClickListener(v -> {
      startActivity(new Intent(DashboardUserActivity.this, ProfileActivity.class));
      finish();
    });
  }

  private void loadCategories() {
    categoryList = new ArrayList<>();

    DatabaseReference ref = FirebaseDatabase.getInstance().getReference("Categories");
    ref.addValueEventListener(new ValueEventListener() {
      @Override
      public void onDataChange(@NonNull DataSnapshot snapshot) {
        categoryList.clear();
        for (DataSnapshot ds : snapshot.getChildren()) {
          Category model = ds.getValue(Category.class);
          categoryList.add(model);
        }
        categoryAdapter = new AdapterCategoryUser(DashboardUserActivity.this, categoryList);
        binding.rvCategories.setAdapter(categoryAdapter);
      }

      @Override
      public void onCancelled(@NonNull DatabaseError error) {

      }
    });
  }

  private void checkUser() {
    FirebaseUser firebaseUser = firebaseAuth.getCurrentUser();

    if (firebaseUser == null) {
      tvDashUserSubTitle.setText("Not Logged In");
    } else {
      String email = firebaseUser.getEmail();
      tvDashUserSubTitle.setText(email);
    }
  }

}