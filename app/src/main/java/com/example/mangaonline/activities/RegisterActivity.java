package com.example.mangaonline.activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Patterns;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.example.mangaonline.R;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.HashMap;
import java.util.Map;

public class RegisterActivity extends AppCompatActivity {

  private String name = "", email = "", password = "", confirmPass = "";

  private EditText edtname;
  private EditText edtEmail;
  private EditText edtPassword;
  private EditText edtConfirmPassword;

  private FirebaseAuth firebaseAuth;

  private ProgressDialog progressDialog;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_register);

    firebaseAuth = FirebaseAuth.getInstance();

    progressDialog = new ProgressDialog(this);
    progressDialog.setTitle("Please Wait!");
    progressDialog.setCanceledOnTouchOutside(false);

    edtname = findViewById(R.id.edt_name_signup);
    edtEmail = findViewById(R.id.edt_email_signup);
    edtPassword = findViewById(R.id.edt_password_signup);
    edtConfirmPassword = findViewById(R.id.edt_confirm_password_signup);

    findViewById(R.id.imb_back).setOnClickListener(v -> onBackPressed());

    findViewById(R.id.btn_reg_layout).setOnClickListener(v -> validateData());
  }

  private void validateData() {
    name = edtname.getText().toString().trim();
    email = edtEmail.getText().toString().trim();
    password = edtPassword.getText().toString().trim();
    confirmPass = edtConfirmPassword.getText().toString().trim();

    if (TextUtils.isEmpty(name)) {
      Toast.makeText(this, "Name cannot be empty", Toast.LENGTH_SHORT).show();
    } else if (!Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
      Toast.makeText(this, "Invalid Email", Toast.LENGTH_SHORT).show();
    } else if (TextUtils.isEmpty(password)) {
      Toast.makeText(this, "Password cannot be empty", Toast.LENGTH_SHORT).show();
    } else if (TextUtils.isEmpty(confirmPass)) {
      Toast.makeText(this, "Confirm Password cannot be empty", Toast.LENGTH_SHORT).show();
    } else if (!password.equals(confirmPass)) {
      Toast.makeText(this, "Confirm Password incorrect", Toast.LENGTH_SHORT).show();
    } else {
      createNewUser();
    }
  }

  private void createNewUser() {
    progressDialog.setMessage("Creating new account...");
    progressDialog.show();

    firebaseAuth.createUserWithEmailAndPassword(email, password)
        .addOnSuccessListener(authResult -> updateUserInfo())
        .addOnFailureListener(exception -> {
          progressDialog.dismiss();
          Toast.makeText(this, "Create new account failed!", Toast.LENGTH_SHORT).show();
        });
  }

  private void updateUserInfo() {
    progressDialog.setMessage("Saving user info...");

    long timestamp = System.currentTimeMillis();

    String uid = firebaseAuth.getUid();

    Map<String, Object> map = new HashMap<>();
    map.put("uid", uid);
    map.put("email", email);
    map.put("name", name);
    map.put("password", password);
    map.put("avatar", "");
    map.put("type", "user");
    map.put("timestamp", timestamp);

    DatabaseReference ref = FirebaseDatabase.getInstance().getReference("Users");
    ref.child(uid)
        .setValue(map)
        .addOnSuccessListener(unused -> {
          progressDialog.dismiss();
          Toast.makeText(this, "Created Successful", Toast.LENGTH_SHORT).show();
          startActivity(new Intent(RegisterActivity.this, DashboardUserActivity.class));
          finish();
        })
        .addOnFailureListener(exception -> {
          progressDialog.dismiss();
          Toast.makeText(this, "Created failed", Toast.LENGTH_SHORT).show();
        });
  }
}