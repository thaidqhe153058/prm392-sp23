package com.example.mangaonline.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;

import com.example.mangaonline.R;

public class MainActivity extends AppCompatActivity {

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_main);

    findViewById(R.id.btn_login).setOnClickListener(v -> {
      startActivity(new Intent(MainActivity.this, LoginActivity.class));
    });

    findViewById(R.id.btn_skip).setOnClickListener(v -> {
      startActivity(new Intent(MainActivity.this, DashboardUserActivity.class));
    });
  }
}