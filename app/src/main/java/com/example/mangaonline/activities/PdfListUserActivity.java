package com.example.mangaonline.activities;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.example.mangaonline.adapters.AdapterPdfAdmin;
import com.example.mangaonline.adapters.AdapterPdfUser;
import com.example.mangaonline.databinding.ActivityPdfListUserBinding;
import com.example.mangaonline.models.Pdf;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

public class PdfListUserActivity extends AppCompatActivity {
  private ActivityPdfListUserBinding binding;

  private List<Pdf> pdfArrayList;

  private AdapterPdfUser adapterPdfUser;

  private String categoryId, categoryTitle;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    binding = ActivityPdfListUserBinding.inflate(getLayoutInflater());
    setContentView(binding.getRoot());

    Intent intent = getIntent();
    categoryId = intent.getStringExtra("categoryId");
    categoryTitle = intent.getStringExtra("categoryTitle");

    binding.subTitleTv.setText(categoryTitle);

    loadPdfList();

    binding.searchEt.addTextChangedListener(new TextWatcher() {
      @Override
      public void beforeTextChanged(CharSequence s, int start, int count, int after) {

      }

      @Override
      public void onTextChanged(CharSequence s, int start, int before, int count) {
        try {
          adapterPdfUser.getFilter().filter(s);
        } catch (Exception ignored) {
        }
      }

      @Override
      public void afterTextChanged(Editable s) {

      }
    });

    binding.backBtn.setOnClickListener(v -> onBackPressed());
  }

  private void loadPdfList() {
    pdfArrayList = new ArrayList<>();

    DatabaseReference ref = FirebaseDatabase.getInstance().getReference("Books");
    ref.orderByChild("categoryId").equalTo(categoryId)
        .addValueEventListener(new ValueEventListener() {
          @Override
          public void onDataChange(@NonNull DataSnapshot snapshot) {
            pdfArrayList.clear();
            for (DataSnapshot ds : snapshot.getChildren()) {
              Pdf model = ds.getValue(Pdf.class);
              pdfArrayList.add(model);
            }

            adapterPdfUser = new AdapterPdfUser(PdfListUserActivity.this, pdfArrayList);
            binding.bookRv.setAdapter(adapterPdfUser);
          }

          @Override
          public void onCancelled(@NonNull DatabaseError error) {

          }
        });
  }
}