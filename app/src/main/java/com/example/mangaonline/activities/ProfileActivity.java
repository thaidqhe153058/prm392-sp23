package com.example.mangaonline.activities;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import androidx.recyclerview.widget.RecyclerView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import com.bumptech.glide.Glide;
import com.example.mangaonline.R;
import com.example.mangaonline.adapters.AdapterPdfFavorite;
import com.example.mangaonline.databinding.ActivityProfileBinding;
import com.example.mangaonline.models.Pdf;
import com.example.mangaonline.utils.MyApplication;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

public class ProfileActivity extends AppCompatActivity {

  private ActivityProfileBinding binding;

  private FirebaseAuth firebaseAuth;

  private FirebaseUser firebaseUser;

  private ProgressDialog progressDialog;

  private ArrayList<Pdf> pdfArrayList;

  private AdapterPdfFavorite adapterPdfFavorite;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    binding = ActivityProfileBinding.inflate(getLayoutInflater());
    setContentView(R.layout.activity_profile);

    TextView a = (TextView) findViewById(R.id.accountStatusTv);
    a.setText("N/A");

    firebaseAuth = FirebaseAuth.getInstance();
    firebaseUser = firebaseAuth.getCurrentUser();
    loadUserInfo();
    loadFavoriteBooks();
    progressDialog = new ProgressDialog(this);
    progressDialog.setTitle("Wait...");
    progressDialog.setCanceledOnTouchOutside(false);



    findViewById(R.id.imb_edit_profile).setOnClickListener(v -> {
      startActivity(new Intent(ProfileActivity.this, ProfileEditActivity.class));
    });

    findViewById(R.id.imb_back_profile).setOnClickListener(v -> onBackPressed());

    a.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        if (firebaseUser.isEmailVerified()) {
          Toast.makeText(ProfileActivity.this, "Account is verify", Toast.LENGTH_SHORT).show();
        } else {
          emailVericationDialog();
        }
      }
    });
  }

  private void emailVericationDialog() {
    AlertDialog.Builder builder = new AlertDialog.Builder(this);
    builder.setTitle("Verify Email")
        .setMessage("Are you want send email to confirm?")
        .setPositiveButton("SEND", new DialogInterface.OnClickListener() {
          @Override
          public void onClick(DialogInterface dialog, int which) {
            sendEmailVerification();
          }
        }).setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
          @Override
          public void onClick(DialogInterface dialog, int which) {
            dialog.dismiss();
          }
        }).show();
  }

  private void sendEmailVerification() {
    progressDialog.setMessage("Sending email confirm in your email" + firebaseUser.getEmail());

    Task<Void> voidTask = firebaseUser.sendEmailVerification()
        .addOnSuccessListener(new OnSuccessListener<Void>() {
          @Override
          public void onSuccess(Void unused) {
            progressDialog.dismiss();
            Toast.makeText(ProfileActivity.this, "Check your mail:" + firebaseUser.getEmail(),
                Toast.LENGTH_SHORT).show();
          }
        }).addOnFailureListener(new OnFailureListener() {
          @Override
          public void onFailure(@NonNull Exception e) {
            progressDialog.dismiss();
            Toast.makeText(ProfileActivity.this, "Fail", Toast.LENGTH_SHORT).show();
          }
        });
  }

  private void loadUserInfo() {
    TextView a = (TextView) findViewById(R.id.accountStatusTv);

    if (firebaseUser.isEmailVerified()) {
      a.setText("Verified");
    } else {
      a.setText("Not Verified");
    }

    DatabaseReference ref = FirebaseDatabase.getInstance().getReference("Users");
    ref.child(firebaseAuth.getUid())
        .addValueEventListener(new ValueEventListener() {
          @Override
          public void onDataChange(@NonNull DataSnapshot snapshot) {
            String email = snapshot.child("email").getValue() + "";
            String name = snapshot.child("name").getValue() + "";
            String avatar = snapshot.child("avatar").getValue() + "";
            String timestamp = snapshot.child("timestamp").getValue() + "";
            String type = snapshot.child("type").getValue() + "";

            String formatDate = MyApplication.formatTimestamp(Long.parseLong(timestamp));

            ((TextView) findViewById(R.id.tv_email_profile)).setText(email);
            ((TextView) findViewById(R.id.tv_name_profile)).setText(name);
            ((TextView) findViewById(R.id.tv_member_date)).setText(formatDate);
            ((TextView) findViewById(R.id.tv_account_type)).setText(type);

            Glide.with(ProfileActivity.this)
                .load(avatar)
                .placeholder(R.drawable.ic_person_gray)
                .into((ImageView) findViewById(R.id.iv_avatar));
          }

          @Override
          public void onCancelled(@NonNull DatabaseError error) {
          }
        });
  }

  private void loadFavoriteBooks() {
    pdfArrayList = new ArrayList<>();
    TextView favBookCount = (TextView) findViewById(R.id.tv_favorite_book_count);
    RecyclerView favBookView = (RecyclerView) findViewById(R.id.booksRv);

      DatabaseReference ref = FirebaseDatabase.getInstance().getReference("Users");
    ref.child(firebaseAuth.getUid()).child("Favorites")
        .addValueEventListener(new ValueEventListener() {
          @Override
          public void onDataChange(@NonNull DataSnapshot snapshot) {
            pdfArrayList.clear();
            for (DataSnapshot ds : snapshot.getChildren()) {
              String bookId = "" + ds.child("bookId").getValue();

              Pdf modelPdf = new Pdf();
              modelPdf.setId(bookId);

              pdfArrayList.add(modelPdf);
            }
              favBookCount.setText("" + pdfArrayList.size());
              adapterPdfFavorite = new AdapterPdfFavorite(ProfileActivity.this, pdfArrayList);
              favBookView.setAdapter(adapterPdfFavorite);

          }

          @Override
          public void onCancelled(@NonNull DatabaseError error) {

          }
        });
  }
}