package com.example.mangaonline.activities;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.provider.SyncStateContract;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.example.mangaonline.R;
import com.example.mangaonline.utils.Constants;
import com.github.barteksc.pdfviewer.PDFView;
import com.github.barteksc.pdfviewer.listener.OnErrorListener;
import com.github.barteksc.pdfviewer.listener.OnPageChangeListener;
import com.github.barteksc.pdfviewer.listener.OnPageErrorListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

public class PdfViewActivity extends AppCompatActivity {
  private String bookId = "";
  private static final String TAG = "PDF_VIEW_TAG";

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_pdf_view);

    Intent intent = getIntent();
    bookId = intent.getStringExtra("bookId");

    loadBookDetails();

    findViewById(R.id.backBtn).setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        onBackPressed();
      }
    });
  }

  private void loadBookDetails() {
    DatabaseReference ref = FirebaseDatabase.getInstance().getReference("Books");
    ref.child(bookId).addListenerForSingleValueEvent(new ValueEventListener() {
      @Override
      public void onDataChange(@NonNull DataSnapshot snapshot) {
        String pdfUrl = "" + snapshot.child("url").getValue();

        loadBookFormUrl(pdfUrl);
      }

      @Override
      public void onCancelled(@NonNull DatabaseError error) {

      }
    });
  }

  private void loadBookFormUrl(String pdfUrl) {
    StorageReference reference = FirebaseStorage.getInstance().getReferenceFromUrl(pdfUrl);

    reference.getBytes(Constants.MAX_BYTES_PDF).addOnSuccessListener(new OnSuccessListener<byte[]>() {
      @Override
      public void onSuccess(byte[] bytes) {

        ((PDFView) findViewById(R.id.pdfView1))
            .fromBytes(bytes)
            .swipeHorizontal(false)
            .onPageChange(new OnPageChangeListener() {
              @Override
              public void onPageChanged(int page, int pageCount) {
                int currentPage = (page + 1);
                ((TextView) findViewById(R.id.toolbarSubtitleTv)).setText(currentPage + "/" + pageCount);
                Log.d(TAG, "onPageChanged" + currentPage + "/" + pageCount);
              }
            }).onError(new OnErrorListener() {
              @Override
              public void onError(Throwable t) {
                Log.d(TAG, "onError" + t.getMessage());
                Toast.makeText(PdfViewActivity.this, "" + t.getMessage(), Toast.LENGTH_SHORT).show();
              }
            }).onPageError(new OnPageErrorListener() {
              @Override
              public void onPageError(int page, Throwable t) {
                Log.d(TAG, "onPageError" + t.getMessage());
                Toast.makeText(PdfViewActivity.this, "Error on page " + page + " " + t.getMessage(),
                    Toast.LENGTH_SHORT).show();
              }
            })
            .load();
        findViewById(R.id.progressBar).setVisibility(View.GONE);
      }
    }).addOnFailureListener(new OnFailureListener() {
      @Override
      public void onFailure(@NonNull Exception e) {
        Log.d(TAG, "onFailure" + e.getMessage());
        findViewById(R.id.progressBar).setVisibility(View.GONE);
      }
    });
  }
}