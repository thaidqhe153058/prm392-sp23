package com.example.mangaonline.activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Patterns;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.example.mangaonline.R;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class LoginActivity extends AppCompatActivity {

  private FirebaseAuth firebaseAuth;

  private ProgressDialog progressDialog;

  private String email = "", password = "";

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_login);

    firebaseAuth = FirebaseAuth.getInstance();

    progressDialog = new ProgressDialog(this);
    progressDialog.setTitle("Please Wait!");
    progressDialog.setCanceledOnTouchOutside(false);

    findViewById(R.id.tv_no_account).setOnClickListener(v -> {
      startActivity(new Intent(LoginActivity.this, RegisterActivity.class));
    });

    findViewById(R.id.btn_login_login_layout).setOnClickListener(v -> {
      validateData();
    });

    findViewById(R.id.tv_forgot_password).setOnClickListener(v -> {
      startActivity(new Intent(LoginActivity.this, ForgotPasswordActivity.class));
    });
  }

  private void validateData() {
    email = ((TextView) findViewById(R.id.edt_email_login)).getText().toString().trim();
    password = ((TextView) findViewById(R.id.edt_password_login)).getText().toString().trim();

    if (!Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
      Toast.makeText(this, "Invalid Email", Toast.LENGTH_SHORT).show();
    } else if (TextUtils.isEmpty(password)) {
      Toast.makeText(this, "Password cannot be empty", Toast.LENGTH_SHORT).show();
    } else {
      loginUser();
    }
  }

  private void loginUser() {
    progressDialog.setMessage("Logging in...");
    progressDialog.show();

    firebaseAuth.signInWithEmailAndPassword(email, password).addOnSuccessListener(authResult -> {
      checkUserType();
    }).addOnFailureListener(exception -> {
      progressDialog.dismiss();
      Toast.makeText(this, "Login failed", Toast.LENGTH_SHORT).show();
    });
  }

  private void checkUserType() {
    progressDialog.setMessage("Checking user...");

    FirebaseUser firebaseUser = firebaseAuth.getCurrentUser();

    DatabaseReference ref = FirebaseDatabase.getInstance().getReference("Users");
    ref.child(firebaseAuth.getUid()).addListenerForSingleValueEvent(new ValueEventListener() {
      @Override
      public void onDataChange(DataSnapshot snapshot) {
        progressDialog.dismiss();

        String userType = (String) snapshot.child("type").getValue();

        if (userType.equals("admin")) {
          startActivity(new Intent(LoginActivity.this, DashboardAdminActivity.class));
          finish();
        } else if (userType.equals("user")) {
          startActivity(new Intent(LoginActivity.this, DashboardUserActivity.class));
          finish();
        }
      }

      @Override
      public void onCancelled(DatabaseError error) {
      }
    });
  }
}