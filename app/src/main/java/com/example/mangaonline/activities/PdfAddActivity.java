package com.example.mangaonline.activities;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.example.mangaonline.databinding.ActivityPdfAddBinding;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class PdfAddActivity extends AppCompatActivity {

  private ActivityPdfAddBinding binding;

  private FirebaseAuth firebaseAuth;

  private ProgressDialog progressDialog;

  private List<String> categoryTitleArrayList, categoryIdArrayList;

  private String selectedCategoryId, selectedCategoryTitle;

  private Uri pdfUri = null;

  private String title = "";
  private String desc = "";

  private static final int PDF_PICK_CODE = 1000;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    binding = ActivityPdfAddBinding.inflate(getLayoutInflater());
    setContentView(binding.getRoot());

    firebaseAuth = FirebaseAuth.getInstance();
    loadCategories();

    progressDialog = new ProgressDialog(this);
    progressDialog.setTitle("Please Wait...");
    progressDialog.setCanceledOnTouchOutside(false);

    binding.imbBackAddBook.setOnClickListener(v -> onBackPressed());

    binding.imbAttach.setOnClickListener(v -> selectPDFFile());

    binding.tvAddBookCategory.setOnClickListener(v -> categoryPickDialog());

    binding.btnAddBook.setOnClickListener(v -> validateData());
  }

  private void validateData() {
    title = binding.edtBookTitle.getText().toString().trim();
    desc = binding.edtBookDesc.getText().toString().trim();

    if (TextUtils.isEmpty(title)) {
      Toast.makeText(this, "Title cannot be empty", Toast.LENGTH_SHORT).show();
    } else if (TextUtils.isEmpty(desc)) {
      Toast.makeText(this, "Please Enter Description", Toast.LENGTH_SHORT).show();
    } else if (TextUtils.isEmpty(selectedCategoryTitle)) {
      Toast.makeText(this, "Please Choose an Category", Toast.LENGTH_SHORT).show();
    } else if (pdfUri == null) {
      Toast.makeText(this, "Please attach an pdf file", Toast.LENGTH_SHORT).show();
    } else {
      uploadBook();
    }
  }

  private void uploadBook() {
    progressDialog.setMessage("Uploading...");
    progressDialog.show();

    long timestamp = System.currentTimeMillis();

    String filePath = "Books/" + timestamp;
    StorageReference ref = FirebaseStorage.getInstance().getReference(filePath);
    ref.putFile(pdfUri)
        .addOnSuccessListener(taskSnapshot -> {
          Task<Uri> uriTask = taskSnapshot.getStorage().getDownloadUrl();
          while (!uriTask.isSuccessful()) ;
          String uploadedPdfUrl = String.valueOf(uriTask.getResult());
          uploadPdfIntoDb(uploadedPdfUrl, timestamp);
        })
        .addOnFailureListener(e -> {
          Toast.makeText(PdfAddActivity.this, "Upload book failed: " + e.getMessage(),
              Toast.LENGTH_SHORT).show();
        });
  }

  private void uploadPdfIntoDb(String uploadedPdfUrl, long timestamp) {
    progressDialog.setMessage("Uploading pdf info...");

    String uid = firebaseAuth.getUid();

    Map<String, Object> map = new HashMap<>();
    map.put("uid", uid);
    map.put("id", String.valueOf(timestamp));
    map.put("title", title);
    map.put("description", desc);
    map.put("categoryId", selectedCategoryId);
    map.put("url", uploadedPdfUrl);
    map.put("timestamp", timestamp);
    map.put("viewsCount", 0);
    map.put("downloadsCount", 0);

    DatabaseReference ref = FirebaseDatabase.getInstance().getReference("Books");
    ref.child("" + timestamp)
        .setValue(map)
        .addOnSuccessListener(unused -> {
          progressDialog.dismiss();
          Toast.makeText(this, "Add data successfully!",
              Toast.LENGTH_SHORT).show();
        })
        .addOnFailureListener(e -> {
          progressDialog.dismiss();
          Toast.makeText(this, "Failed while add data into db:" + e.getMessage(),
              Toast.LENGTH_SHORT).show();
        });
  }

  private void loadCategories() {
    categoryTitleArrayList = new ArrayList<>();
    categoryIdArrayList = new ArrayList<>();

    DatabaseReference ref = FirebaseDatabase.getInstance().getReference("Categories");
    ref.addListenerForSingleValueEvent(new ValueEventListener() {
      @Override
      public void onDataChange(@NonNull DataSnapshot snapshot) {
        categoryTitleArrayList.clear();
        categoryIdArrayList.clear();
        for (DataSnapshot ds : snapshot.getChildren()) {

          String categoryId = "" + ds.child("id").getValue();
          String categoryTitle = "" + ds.child("category").getValue();

          categoryTitleArrayList.add(categoryTitle);
          categoryIdArrayList.add(categoryId);
        }
      }

      @Override
      public void onCancelled(@NonNull DatabaseError error) {
      }
    });
  }

  private void categoryPickDialog() {
    String[] cateArr = new String[categoryTitleArrayList.size()];
    for (int i = 0; i < categoryTitleArrayList.size(); i++) {
      cateArr[i] = categoryTitleArrayList.get(i);
    }

    AlertDialog.Builder builder = new AlertDialog.Builder(this);
    builder.setTitle("Pick Category")
        .setItems(cateArr, (dialog, which) -> {
          selectedCategoryTitle = categoryTitleArrayList.get(which);
          selectedCategoryId = categoryIdArrayList.get(which);

          binding.tvAddBookCategory.setText(selectedCategoryTitle);
        })
        .show();
  }

  private void selectPDFFile() {
    Intent intent = new Intent();
    intent.setType("application/pdf");
    intent.setAction(Intent.ACTION_GET_CONTENT);
    startActivityForResult(Intent.createChooser(intent, "Select PDF"), PDF_PICK_CODE);
  }

  @Override
  protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
    super.onActivityResult(requestCode, resultCode, data);

    if (resultCode == RESULT_OK) {
      if (requestCode == PDF_PICK_CODE) {
        pdfUri = data.getData();
      }
    }
  }
}