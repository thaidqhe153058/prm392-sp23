package com.example.mangaonline.activities;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.example.mangaonline.R;
import com.example.mangaonline.adapters.AdapterCategoryAdmin;
import com.example.mangaonline.databinding.ActivityDashboardAdminBinding;
import com.example.mangaonline.models.Category;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

public class DashboardAdminActivity extends AppCompatActivity {

  private FirebaseAuth firebaseAuth;

  private TextView tvDashAdminSubTitle;

  private ActivityDashboardAdminBinding binding;

  private List<Category> categoryList;

  private AdapterCategoryAdmin categoryAdapter;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    binding = ActivityDashboardAdminBinding.inflate(getLayoutInflater());
    setContentView(binding.getRoot());

    firebaseAuth = FirebaseAuth.getInstance();

    tvDashAdminSubTitle = findViewById(R.id.tv_dash_admin_sub_title);

    checkUser();
    loadCategories();

    binding.edtSearchCategory.addTextChangedListener(new TextWatcher() {
      @Override
      public void beforeTextChanged(CharSequence s, int start, int count, int after) {
      }

      @Override
      public void onTextChanged(CharSequence s, int start, int before, int count) {
        try {
          categoryAdapter.getFilter().filter(s);
        } catch (Exception ignored) {
        }
      }

      @Override
      public void afterTextChanged(Editable s) {
      }
    });

    findViewById(R.id.imb_logout_admin).setOnClickListener(v -> {
      firebaseAuth.signOut();
      checkUser();
    });

    //handle click, start category and screen
    findViewById(R.id.addCategoryBtn).setOnClickListener(v -> onAddCategory());

    // open profile onclick
    findViewById(R.id.imb_profile_admin).setOnClickListener(v -> {
      startActivity(new Intent(DashboardAdminActivity.this, ProfileActivity.class));
    });

    binding.addPdfFab.setOnClickListener(v -> {
      startActivity(new Intent(DashboardAdminActivity.this, PdfAddActivity.class));
    });
  }

  private void loadCategories() {
    categoryList = new ArrayList<>();

    DatabaseReference ref = FirebaseDatabase.getInstance().getReference("Categories");
    ref.addValueEventListener(new ValueEventListener() {
      @Override
      public void onDataChange(@NonNull DataSnapshot snapshot) {
        categoryList.clear();
        for (DataSnapshot ds : snapshot.getChildren()) {
          Category model = ds.getValue(Category.class);
          categoryList.add(model);
        }
        categoryAdapter = new AdapterCategoryAdmin(DashboardAdminActivity.this, categoryList);
        binding.rvCategories.setAdapter(categoryAdapter);
      }

      @Override
      public void onCancelled(@NonNull DatabaseError error) {

      }
    });
  }

  private void onAddCategory() {
    startActivity(new Intent(DashboardAdminActivity.this, CategoryAddActivity.class));
  }

  private void checkUser() {
    FirebaseUser firebaseUser = firebaseAuth.getCurrentUser();

    if (firebaseUser == null) {
      startActivity(new Intent(DashboardAdminActivity.this, MainActivity.class));
      finish();
    } else {
      String email = firebaseUser.getEmail();
      tvDashAdminSubTitle.setText(email);
    }
  }

}