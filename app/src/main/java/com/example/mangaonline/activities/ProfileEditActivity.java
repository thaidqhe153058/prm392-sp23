package com.example.mangaonline.activities;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.view.Menu;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.PopupMenu;

import com.bumptech.glide.Glide;
import com.example.mangaonline.R;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.util.HashMap;
import java.util.Map;

public class ProfileEditActivity extends AppCompatActivity {

  private FirebaseAuth firebaseAuth;

  private ProgressDialog progressDialog;

  private Uri imageUri = null;

  private ImageView avatar;

  private String name = "";

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_profile_edit);

    avatar = findViewById(R.id.iv_avatar_edit);

    progressDialog = new ProgressDialog(this);
    progressDialog.setTitle("Please wait...");
    progressDialog.setCanceledOnTouchOutside(false);

    firebaseAuth = FirebaseAuth.getInstance();
    loadUserInfo();

    findViewById(R.id.imb_back_profile_edit).setOnClickListener(v -> onBackPressed());

    avatar.setOnClickListener(v -> {
      showImageAttachMenu();
    });

    findViewById(R.id.btn_edit).setOnClickListener(v -> {
      validateData();
      startActivity(new Intent(ProfileEditActivity.this, ProfileActivity.class));
    });

    findViewById(R.id.imb_back_profile_edit).setOnClickListener(v -> onBackPressed());
  }

  private void validateData() {
    name = ((EditText) findViewById(R.id.edt_name_edit)).getText().toString().trim();
    if (TextUtils.isEmpty(name)) {
      Toast.makeText(this, "Field cannot be empty", Toast.LENGTH_SHORT).show();
    } else {
      if (imageUri == null) {
        updateProfile("");
      } else {
        uploadImage();
      }
    }
  }

  private void uploadImage() {
    progressDialog.setMessage("Updating avatar...");
    progressDialog.show();

    String path = "ProfileImages/" + firebaseAuth.getUid();

    StorageReference ref = FirebaseStorage.getInstance().getReference(path);
    ref.putFile(imageUri)
        .addOnSuccessListener(taskSnapshot -> {
          Task<Uri> uriTask = taskSnapshot.getStorage().getDownloadUrl();
          while (!uriTask.isSuccessful()) ;
          String uploadedImageUrl = String.valueOf(uriTask.getResult());
          updateProfile(uploadedImageUrl);
        })
        .addOnFailureListener(e -> {
          progressDialog.dismiss();
          Toast.makeText(this, "Upload image failed", Toast.LENGTH_SHORT).show();
        });
  }

  private void updateProfile(String imageUrl) {
    progressDialog.setMessage("Updating user profile...");
    progressDialog.show();

    Map<String, Object> map = new HashMap<>();
    map.put("name", name);
    if (imageUri != null) {
      map.put("avatar", imageUrl);
    }

    DatabaseReference ref = FirebaseDatabase.getInstance().getReference("Users");
    ref.child(firebaseAuth.getUid())
        .updateChildren(map)
        .addOnSuccessListener(unused -> {
          progressDialog.dismiss();
          Toast.makeText(this, "Profile updated successful",
              Toast.LENGTH_SHORT).show();
        })
        .addOnFailureListener(e -> {
          progressDialog.dismiss();
          Toast.makeText(this, "Failed to update profile due to " + e.getMessage(),
              Toast.LENGTH_SHORT).show();
        });
  }

  private void loadUserInfo() {
    DatabaseReference ref = FirebaseDatabase.getInstance().getReference("Users");
    ref.child(firebaseAuth.getUid()).addValueEventListener(new ValueEventListener() {
      @Override
      public void onDataChange(@NonNull DataSnapshot snapshot) {
        String name = (String) snapshot.child("name").getValue();
        String avatarStr = (String) snapshot.child("avatar").getValue();

        ((TextView) findViewById(R.id.edt_name_edit)).setText(name);

        Glide.with(ProfileEditActivity.this)
            .load(avatarStr)
            .placeholder(R.drawable.ic_person_gray)
            .into(avatar);
      }

      @Override
      public void onCancelled(@NonNull DatabaseError error) {
      }
    });
  }

  private void showImageAttachMenu() {
    PopupMenu popupMenu = new PopupMenu(this, avatar);
    popupMenu.getMenu().add(Menu.NONE, 0, 0, "Camera");
    popupMenu.getMenu().add(Menu.NONE, 1, 1, "Gallery");

    popupMenu.show();

    popupMenu.setOnMenuItemClickListener(item -> {
      int which = item.getItemId();
      if (which == 0) {
        pickImageFromCamera();
      } else if (which == 1) {
        pickImageFromGallery();
      }
      return false;
    });
  }

  private void pickImageFromGallery() {
    Intent intent = new Intent(Intent.ACTION_PICK);
    intent.setType("image/*");
    galleryActivityResultLauncher.launch(intent);
  }

  private void pickImageFromCamera() {
    ContentValues values = new ContentValues();
    values.put(MediaStore.Images.Media.TITLE, "New Pick");
    values.put(MediaStore.Images.Media.DESCRIPTION, "Description");
    imageUri = getContentResolver().insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);

    Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
    intent.putExtra(MediaStore.EXTRA_OUTPUT, imageUri);
    cameraActivityResultLauncher.launch(intent);
  }

  private ActivityResultLauncher<Intent> cameraActivityResultLauncher = registerForActivityResult(
      new ActivityResultContracts.StartActivityForResult(),
      result -> {
        if (result.getResultCode() == Activity.RESULT_OK) {
          avatar.setImageURI(imageUri);
        } else {
          Toast.makeText(this, "Canceled", Toast.LENGTH_SHORT).show();
        }
      }
  );

  private ActivityResultLauncher<Intent> galleryActivityResultLauncher = registerForActivityResult(
      new ActivityResultContracts.StartActivityForResult(),
      result -> {
        if (result.getResultCode() == Activity.RESULT_OK) {
          Intent data = result.getData();
          imageUri = data.getData();
          avatar.setImageURI(imageUri);
        } else {
          Toast.makeText(this, "Canceled", Toast.LENGTH_SHORT).show();
        }
      }
  );
}