package com.example.mangaonline.activities;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.util.Patterns;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.example.mangaonline.R;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;

public class ForgotPasswordActivity extends AppCompatActivity {

  private FirebaseAuth firebaseAuth;
  private ProgressDialog progressDialog;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_forgot_password);

    firebaseAuth = FirebaseAuth.getInstance();

    progressDialog = new ProgressDialog(this);
    progressDialog.setTitle("Please wait");
    progressDialog.setCanceledOnTouchOutside(false);

    findViewById(R.id.backBtn).setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        onBackPressed();
      }
    });

    findViewById(R.id.submitBtn).setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        validateData();
      }
    });
  }

  private String email = "";

  private void validateData() {
    email = ((TextView) findViewById(R.id.emailEt)).getText().toString().trim();

    if (email.isEmpty()) {
      Toast.makeText(this, "Enter Email ...", Toast.LENGTH_SHORT).show();
    } else if (!Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
      Toast.makeText(this, "Invalid email format ...", Toast.LENGTH_SHORT).show();
    } else {
      recoverPassword();
    }
  }

  private void recoverPassword() {
    progressDialog.setMessage("Sending password recover instruction to " + email);
    progressDialog.show();

    firebaseAuth.sendPasswordResetEmail(email)
        .addOnSuccessListener(new OnSuccessListener<Void>() {
          @Override
          public void onSuccess(Void unused) {
            progressDialog.dismiss();
            Toast.makeText(ForgotPasswordActivity.this,
                "Instructions to reset password send to " + email, Toast.LENGTH_SHORT).show();
          }
        })
        .addOnFailureListener(new OnFailureListener() {
          @Override
          public void onFailure(@NonNull Exception e) {
            progressDialog.dismiss();
            Toast.makeText(ForgotPasswordActivity.this, "Failed to send due to" + e.getMessage(),
                Toast.LENGTH_SHORT).show();
          }
        });
  }
}