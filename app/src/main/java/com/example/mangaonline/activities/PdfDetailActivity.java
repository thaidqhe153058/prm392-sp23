package com.example.mangaonline.activities;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import com.example.mangaonline.R;
import com.example.mangaonline.adapters.AdapterComment;
import com.example.mangaonline.databinding.ActivityPdfDetailBinding;
import com.example.mangaonline.databinding.DialogCmtAddBinding;
import com.example.mangaonline.models.Comment;
import com.example.mangaonline.utils.MyApplication;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.HashMap;

public class PdfDetailActivity extends AppCompatActivity {
  String bookId, bookTitle, bookUrl;

  boolean isInMyFavorite = false;

  private FirebaseAuth firebaseAuth;

  private ArrayList<Comment> commentArrayList;

  private AdapterComment adapterComment;

  private static final String TAG_DOWNLOAD = "DOWNLOAD_TAG";

  private ActivityPdfDetailBinding binding;

  private ProgressDialog progressDialog;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    binding = ActivityPdfDetailBinding.inflate(getLayoutInflater());
    setContentView(binding.getRoot());

    Intent intent = getIntent();
    bookId = intent.getStringExtra("bookId");

    progressDialog = new ProgressDialog(this);
    progressDialog.setTitle("Please wait");
    progressDialog.setCanceledOnTouchOutside(false);

    firebaseAuth = FirebaseAuth.getInstance();
    if (firebaseAuth.getCurrentUser() != null) {
      checkIsFavorite();
    }

    loadBookDetails();
    loadComments();
    MyApplication.incrementBookViewCount(bookId);

    findViewById(R.id.backBtn).setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        onBackPressed();
      }
    });

    findViewById(R.id.readBookBtn).setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        Intent intent1 = new Intent(PdfDetailActivity.this, PdfViewActivity.class);
        intent1.putExtra("bookId", bookId);
        startActivity(intent1);
      }
    });

    findViewById(R.id.favoriteBtn).setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        if (firebaseAuth.getCurrentUser() == null) {
          Toast.makeText(PdfDetailActivity.this, "You are not logged in", Toast.LENGTH_SHORT).show();
        } else {
          if (isInMyFavorite) {
            //in favorite, remove from favorite
            MyApplication.removeFromFavorite(PdfDetailActivity.this, bookId);

          } else {
            //not in favorite, add to favorite
            MyApplication.addToFavorite(PdfDetailActivity.this, bookId);
          }
        }
      }
    });

    findViewById(R.id.addCommentBtn).setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        if (firebaseAuth.getCurrentUser() == null) {
          Toast.makeText(PdfDetailActivity.this, "You need login ...", Toast.LENGTH_SHORT).show();
        } else {
          addCommentDialog();
        }
      }
    });
  }

  private void loadComments() {
    commentArrayList = new ArrayList<>();
    Log.d("LoadComments", "loading Comments");
    DatabaseReference ref = FirebaseDatabase.getInstance().getReference("Books");
    ref.child(bookId).child("Comments")
        .addValueEventListener(new ValueEventListener() {
          @Override
          public void onDataChange(@NonNull DataSnapshot snapshot) {
            commentArrayList.clear();
            for (DataSnapshot ds : snapshot.getChildren()) {
              Comment mode = ds.getValue(Comment.class);
              commentArrayList.add(mode);
            }
            adapterComment = new AdapterComment(PdfDetailActivity.this, commentArrayList);
            binding.commentsRv.setAdapter(adapterComment);
          }

          @Override
          public void onCancelled(@NonNull DatabaseError error) {
            Toast.makeText(PdfDetailActivity.this, error.getMessage(), Toast.LENGTH_SHORT).show();
          }
        });
  }

  private String comment = "";

  private void addCommentDialog() {
    DialogCmtAddBinding commentAddBinding = DialogCmtAddBinding.inflate(LayoutInflater.from(this));

    AlertDialog.Builder builder = new AlertDialog.Builder(this, R.style.CustomDialog);
    builder.setView(commentAddBinding.getRoot());

    AlertDialog alertDialog = builder.create();
    alertDialog.show();

    commentAddBinding.backBtn.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        alertDialog.dismiss();
      }
    });

    commentAddBinding.submitBtn.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        comment = commentAddBinding.commentEl.getText().toString().trim();
        if (TextUtils.isEmpty(comment)) {
          Toast.makeText(PdfDetailActivity.this, "Enter your cmt ... ", Toast.LENGTH_SHORT).show();
        } else {
          alertDialog.dismiss();
          addComment();
        }
      }
    });
  }

  private void addComment() {
    progressDialog.setMessage("adding cmt ....");
    progressDialog.show();

    String timestamp = System.currentTimeMillis() + "";

    HashMap<String, Object> hashMap = new HashMap<>();
    hashMap.put("id", "" + timestamp);
    hashMap.put("bookId", "" + bookId);
    hashMap.put("timestamp", "" + timestamp);
    hashMap.put("comment", "" + comment);
    hashMap.put("uid", "" + firebaseAuth.getUid());

    DatabaseReference ref = FirebaseDatabase.getInstance().getReference("Books");
    ref.child(bookId).child("Comments").child(timestamp)
        .setValue(hashMap)
        .addOnSuccessListener(new OnSuccessListener<Void>() {
          @Override
          public void onSuccess(Void unused) {
            Toast.makeText(PdfDetailActivity.this, "Done ", Toast.LENGTH_SHORT).show();
          }
        })
        .addOnFailureListener(e -> {
          Toast.makeText(PdfDetailActivity.this, "Failed to add comment due to:" + e.getMessage()
              , Toast.LENGTH_SHORT).show();
        });
    progressDialog.dismiss();
  }


  private ActivityResultLauncher<String> request =
      registerForActivityResult(new ActivityResultContracts.RequestPermission(), isGranted -> {
        if (isGranted) {
          MyApplication.downloadBook(this, bookId, bookTitle, bookUrl);
        } else {

        }
      });

  private void loadBookDetails() {
    DatabaseReference ref = FirebaseDatabase.getInstance().getReference("Books");
    ref.child(bookId).addListenerForSingleValueEvent(new ValueEventListener() {
      @Override
      public void onDataChange(@NonNull DataSnapshot snapshot) {
        bookTitle = "" + snapshot.child("title").getValue();
        String desc = "" + snapshot.child("description").getValue();
        String viewsCount = "" + snapshot.child("viewsCount").getValue();
        String downloadsCount = "" + snapshot.child("downloadsCount").getValue();
        bookUrl = "" + snapshot.child("url").getValue();
        String timestamp = "" + snapshot.child("timestamp").getValue();
        String categoryId = "" + snapshot.child("categoryId").getValue();

        String date = MyApplication.formatTimestamp(Long.parseLong(timestamp));
        MyApplication.loadCategory("" + categoryId, findViewById(R.id.categoryTv));
        MyApplication.loadPdfFromUrlSinglePage(
            "" + bookUrl, "" + bookTitle, findViewById(R.id.pdfView),
            findViewById(R.id.progressBar), findViewById(R.id.pagesTv));
        MyApplication.loadPdfSize("" + bookUrl, "" + bookTitle, findViewById(R.id.sizeTv));

        ((TextView) findViewById(R.id.titleTv)).setText(bookTitle);
        ((TextView) findViewById(R.id.descriptionTv)).setText(desc);
        ((TextView) findViewById(R.id.viewsTv)).setText(
            viewsCount.replace("null", "N/A"));
        ((TextView) findViewById(R.id.downloadLabelTv)).setText(
            downloadsCount.replace("null", "N/A"));
        ((TextView) findViewById(R.id.dateTv)).setText(date);
      }

      @Override
      public void onCancelled(@NonNull DatabaseError error) {

      }
    });
  }

  private void checkIsFavorite() {
    DatabaseReference reference = FirebaseDatabase.getInstance().getReference("Users");
    reference.child(firebaseAuth.getUid()).child("Favorites").child(bookId)
        .addValueEventListener(new ValueEventListener() {
          @Override
          public void onDataChange(@NonNull DataSnapshot snapshot) {
            isInMyFavorite = snapshot.exists();
            if (isInMyFavorite) {
              ((Button) findViewById(R.id.favoriteBtn)).setCompoundDrawablesRelativeWithIntrinsicBounds(
                  0, R.drawable.ic_favorite_white, 0, 0);
              ((Button) findViewById(R.id.favoriteBtn)).setText("Remove Favorite");
            } else {
              ((Button) findViewById(R.id.favoriteBtn)).setCompoundDrawablesRelativeWithIntrinsicBounds(
                  0, R.drawable.ic_favorite_border_white, 0, 0);
              ((Button) findViewById(R.id.favoriteBtn)).setText("Add Favorite");
            }
          }

          @Override
          public void onCancelled(@NonNull DatabaseError error) {

          }
        });
  }
}